﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Roteiriz.Data.Models.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Services
{
    public class UserContextService : IUserContextService
    {
        private IHttpContextAccessor _contextAccessor;
        public AuthorizationConfig.RoteirizAuthorization RoteirizAuthorization { get; set; }
        public string User { get; set; }
        public UserContextService(IHttpContextAccessor contextAccessor, IOptions<AuthorizationConfig> config)
        {
            _contextAccessor = contextAccessor;
            User = contextAccessor?.HttpContext.User.Identity.Name;
            //User = "roteirizador";
            RoteirizAuthorization = config.Value?.Usuarios?.FirstOrDefault(x => x.Usuario == User)?.RoteirizAuthorization;
        }
    }
}

﻿using Roteiriz.Data.Models.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Services
{
    public interface IUserContextService
    {
        string User { get; set; }
        AuthorizationConfig.RoteirizAuthorization RoteirizAuthorization { get; set; }
    }

}

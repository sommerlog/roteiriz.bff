﻿using Enyim.Caching;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Roteiriz.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Services
{
    public class TokenService
    {
        private TokenModel _middlewareToken;
        private TokenServiceConfig _configuration;
        private IUserContextService _userContext;
        private IMemcachedClient Cache { get; set; }
        private readonly ILogger _logger;

        private string CacheId
        {
            get
            {
                if(_userContext.RoteirizAuthorization != null)
                    return _configuration.CacheId + '_'+ _userContext.RoteirizAuthorization.SiglaProduto;

                return _configuration.CacheId;
            }
        }

        public TokenModel UserToken { get; set; }

        public TokenService(
            IHttpContextAccessor contextAccessor,
            IMemcachedClient cache,
            IOptions<TokenServiceConfig> configuration,
            IUserContextService userContext,
            [FromServices]ILogger<TokenService> logger)
        {
            if (contextAccessor.HttpContext.Request.Headers.ContainsKey("Authorization"))
            {
                var xua = contextAccessor.HttpContext.Request.Headers["Authorization"];

                if(xua.Count > 0)
                {
                    UserToken = new TokenModel() { Token = xua[0].Trim() };
                }
            }

            _configuration = configuration.Value;
            _userContext = userContext;
            Cache = cache;
            _logger = logger;
        }


        public delegate TokenModel TokenEventHandler();
        public event Func<Task> LoginMiddleware;

        public async Task OnLoginMiddleware()
        {
            if (LoginMiddleware == null)
                return;

            var invocationList = LoginMiddleware.GetInvocationList();
            var handlerTasks = new Task[invocationList.Length];

            for (var i = 0; i < invocationList.Length;i++)
            {
                handlerTasks[i] = ((Func<Task>)invocationList[i])();
            }

            await Task.WhenAll(handlerTasks).ConfigureAwait(false);

        }

        public async Task<string> GetAuthorizationString(bool useUserToken = true)
        {
            TokenModel token;
            if (useUserToken)
                token = UserToken ?? MiddlewareToken;
            else
                token = MiddlewareToken;

            if(token == null)
            {
                await OnLoginMiddleware().ConfigureAwait(false);
                token = MiddlewareToken;
            }

            if(token != null)
                return $"{token.Token}";

            return null;

        }


        public TokenModel MiddlewareToken
        {
            get
            {
                if (_middlewareToken == null)
                    _middlewareToken = Cache.Get<TokenModel>(CacheId);

                if (_middlewareToken?.DataExpiracao > Now(_middlewareToken?.DataExpiracao).AddSeconds(10))
                    return _middlewareToken;

                return null;
            }
            set
            {
                if (value?.DataExpiracao > Now(value?.DataExpiracao).AddSeconds(10))
                {
                    var cacheTime = Convert.ToInt32((value?.DataExpiracao - DateTime.UtcNow).Value.TotalSeconds);
                    Cache.Set(CacheId, value, cacheTime);
                    _middlewareToken = value;

                }
            }
        }

        private DateTime Now(DateTime? dateTime)
        {
            if (dateTime?.Kind == DateTimeKind.Local)
                return DateTime.Now;

            return DateTime.UtcNow;
        }
    }
}

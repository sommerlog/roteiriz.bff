﻿using Enyim.Caching.Memcached.Protocol.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models;
using Roteiriz.Data.Models.Config;
using Roteiriz.Data.Models.Roteiriz.Error;
using System.Net;

namespace Roteiriz.BFF.Client
{
    public class BaseClient
    {

        private ApiClient Client { get; set; }
        private ApiEndPointEnum EndPoint{ get; set; }
        private ILogger Logger { get; set; }
        protected ClientConfiguration Configuration { get; private set; }

        public BaseClient(ApiEndPointEnum endPoint,ApiClient client, ILogger logger)
        {
            EndPoint = endPoint;
            Client = client;
            Logger = logger;
            Configuration= client.Configuration;
        }

        protected async Task<TResponse> GetAsync<TResponse, TResponseError>(string action, QueryString? queryParameters = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponse : class
            where TResponseError : BadRequestErroModel
        {
            return await Client.GetAsync<TResponse, TResponseError>(EndPoint, action, queryParameters, Logger, useAuthorization, useUserToken).ConfigureAwait(false);
        }

        protected async Task<TResponse> HeadAsync<TResponse, TResponseError>(string action, QueryString? queryParameters = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponse : class
            where TResponseError : BadRequestErroModel
        {
            return await Client.HeadAsync<TResponse, TResponseError>(EndPoint, action, queryParameters, Logger, useAuthorization, useUserToken).ConfigureAwait(false);
        }

        protected async Task<TResponse> DeleteAsync<TResponse, TResponseError>(string action, QueryString? queryParameters = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponse : class
            where TResponseError : BadRequestErroModel
        {
            return await Client.DeleteAsync<TResponse, TResponseError>(EndPoint, action, queryParameters, Logger, useAuthorization, useUserToken).ConfigureAwait(false);
        }

        protected async Task<TResponse> PostAsync<TResponse, TBody, TResponseError>(string action, TBody body, QueryString? queryParameters = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponseError : BadRequestErroModel
            where TResponse : class
            where TBody : class
        {
            return await Client.PostAsync<TResponse, TBody, TResponseError>(EndPoint, action, body, queryParameters, Logger, useAuthorization, useUserToken).ConfigureAwait(false);
        }

        protected async Task<TResponse> PutAsync<TResponse, TBody, TResponseError>(string action, TBody body, QueryString? queryParameters = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponseError : BadRequestErroModel
            where TResponse : class
            where TBody : class
        {
            return await Client.PutAsync<TResponse, TBody, TResponseError>(EndPoint, action, body, queryParameters, Logger, useAuthorization, useUserToken).ConfigureAwait(false);
        }
    }
}

﻿using Enyim.Caching;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Roteiriz.Data.Base;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models;
using Roteiriz.Data.Models.Config;
using Roteiriz.Data.Models.Roteiriz;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using Roteiriz.Services;
using System;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Reflection.PortableExecutable;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json.Nodes;
using System.Text.Json.Serialization;

namespace Roteiriz.BFF.Client
{
    public class ApiClient
    {
        public ClientConfiguration Configuration { get;private set; }
        public TokenService TokenService { get;private set; }
        public IUserContextService UserContextService { get; set; }
        private IHttpContextAccessor ContextAccessor { get; set; }
        private IMemcachedClient Cache { get; set; }

        public ApiClient(IOptions<ClientConfiguration> configuration, TokenService tokenService, IMemcachedClient cache,IUserContextService userContextService, IHttpContextAccessor contextAccessor)
        {
            Configuration = configuration.Value;
            TokenService = tokenService;
            Cache = cache;
            UserContextService = userContextService;
            TokenService.LoginMiddleware += TokenService_LoginMiddleware;
            ContextAccessor = contextAccessor;
        }

        private async Task TokenService_LoginMiddleware()
        {
            var model = new LoginRequest(
                UserContextService.RoteirizAuthorization.Usuario, 
                UserContextService.RoteirizAuthorization.Senha,
                UserContextService.RoteirizAuthorization.CodigoProduto);

            var response = await PostAsync<LoginResponseModel, LoginRequest, BadRequestErroModel>
                (ApiEndPointEnum.AuthSchema, "user/Login", model,useAuthorization: true, useUserToken: true);

            TokenService.MiddlewareToken = new TokenModel
            {
                Token = response.user.accessToken.token,
                DataExpiracao = response.user.accessToken.expirationDate
            };
        }

        private string GetEndPoint(ApiEndPointEnum apiEndPoint)
        {
            try
            {
                return Configuration.Endpoints.GetType().GetProperty(apiEndPoint.ToString()).GetValue(Configuration.Endpoints).ToString();
            }
            catch(Exception e)
            {
                return null;
            }
        }

        private HttpClient CreateClient(ApiEndPointEnum endPoint, ILogger logger)
        {

            var endpoint = GetEndPoint(endPoint);

            if (string.IsNullOrWhiteSpace(endpoint))
            {
                throw new ArgumentException("Invalid URI", "endPoint");
            }
            endpoint = endpoint.EndsWith("/") ? endpoint : $"{endpoint}/";


            var baseAddress = Configuration.BaseAddress;
            if (!string.IsNullOrEmpty(baseAddress))
            {
                baseAddress = baseAddress.EndsWith("/") ? baseAddress : $"{baseAddress}/";
            }

            var contentType = new MediaTypeWithQualityHeaderValue(Configuration.ContentType);
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            var httpClient = new HttpClient(clientHandler)
            {
                BaseAddress = new Uri(endpoint + baseAddress, UriKind.Absolute),
                Timeout = Configuration.RequestTimeout
            };
            httpClient.DefaultRequestHeaders.Accept.Add(contentType);
            httpClient.DefaultRequestHeaders.Add("User-Agent", Configuration.UserAgent);

            return httpClient;
        }
        
        private Uri GetUri(HttpClient httpClient, string action)
        {
            var uri = new Uri(httpClient.BaseAddress, action);

            if(Configuration.Replaces.Length > 0)
            {
                var url = uri.OriginalString;

                if (!action.Contains("v2"))
                {
                    foreach(var item in Configuration.Replaces)
                    {
                        url = url.Replace($"{{{item.From}}}", item.To);
                    }
                }
                else
                {
                    url = url.Replace("v{version}", "");

                }
                uri = new Uri(url);
            }
            return uri;
        }

        private async Task<TResponse> RequestAsync<TResponse, TBody, TResponseError>(HttpMethod httpMethod, ApiEndPointEnum endPoint, string action, QueryString? queryParameters = null, TBody body = null, ILogger logger = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponse : class
            where TBody : class
            where TResponseError: BadRequestErroModel
        {
            var httpClient = CreateClient(endPoint,logger);
            var method = httpMethod.ToString().ToUpper();
            var uri = GetUri(httpClient, action);
            var response = new HttpResponseMessage();

            var strBody = string.Empty;

            try
            {
                if(queryParameters != null)
                {
                    uri = new Uri(uri, queryParameters.Value.Value);
                }

                httpClient.DefaultRequestHeaders.Remove("Authorization");
                if (useAuthorization)
                {
                    var token = await TokenService.GetAuthorizationString().ConfigureAwait(false);
                    if(token != null) 
                    { 
                        httpClient.DefaultRequestHeaders.Add("Authorization", token);
                    }
                }

                switch (method)
                {
                    case "GET":
                    case "HEAD":
                        if(body != null)
                        {
                            throw new ArgumentException("HEAD or GET shoud never have a body", "body");
                        }
                        response = await httpClient.GetAsync(uri).ConfigureAwait(false);
                        break;
                    case "DELETE":
                        response = await httpClient.DeleteAsync(uri).ConfigureAwait(false);
                        break;
                    case "POST":
                    case "PUT":
                        using(var ms = new MemoryStream())
                        {
                            if(method == "POST")
                            {
                                response = await httpClient.PostAsJsonAsync(uri, body).ConfigureAwait(false);
                            }
                            else
                            {
                                response = await httpClient.PutAsJsonAsync(uri, body).ConfigureAwait(false);
                            }
                        }
                    break;
                    default:
                        throw new NotImplementedException("Method now Allowed");
                }


                var json = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                    return JsonConvert.DeserializeObject<TResponse>(json);


                var objError = JsonConvert.DeserializeObject<TResponseError>(json);

                throw ErrorResponse(objError);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private Exception ErrorResponse<TResponseError>(TResponseError response)
            where TResponseError : BadRequestErroModel
        {
            return new BaseApiException<TResponseError>(response);
        }

        public async Task<TResponse> GetAsync<TResponse, TResponseError>(ApiEndPointEnum endPoint, string action, QueryString? queryParameters = null, ILogger logger = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponse : class
            where TResponseError : BadRequestErroModel
        {
            return await this.RequestAsync<TResponse, TResponse, TResponseError>(HttpMethod.Get, endPoint, action,queryParameters, null, logger, useAuthorization, useUserToken).ConfigureAwait(false);
        }

        public async Task<TResponse> HeadAsync<TResponse, TResponseError>(ApiEndPointEnum endPoint, string action, QueryString? queryParameters = null, ILogger logger = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponse : class
            where TResponseError : BadRequestErroModel
        {
            return await this.RequestAsync<TResponse, TResponse, TResponseError>(HttpMethod.Head, endPoint, action, queryParameters, null, logger, useAuthorization, useUserToken).ConfigureAwait(false);
        }

        public async Task<TResponse> DeleteAsync<TResponse, TResponseError>(ApiEndPointEnum endPoint, string action, QueryString? queryParameters = null, ILogger logger = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponse : class
            where TResponseError : BadRequestErroModel
        {
            return await this.RequestAsync<TResponse, TResponse, TResponseError>(HttpMethod.Delete, endPoint, action, queryParameters, null, logger, useAuthorization, useUserToken).ConfigureAwait(false);
        }

        public async Task<TResponse> PostAsync<TResponse,TBody, TResponseError>(ApiEndPointEnum endPoint, string action, TBody body,  QueryString? queryParameters = null, ILogger logger = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponse : class
            where TBody : class
            where TResponseError : BadRequestErroModel
        {
            return await this.RequestAsync<TResponse, TBody, TResponseError>(HttpMethod.Post, endPoint, action, queryParameters, body, logger, useAuthorization, useUserToken).ConfigureAwait(false);
        }

        public async Task<TResponse> PutAsync<TResponse, TBody, TResponseError>(ApiEndPointEnum endPoint, string action, TBody body, QueryString? queryParameters = null, ILogger logger = null, bool useAuthorization = true, bool useUserToken = true)
            where TResponse : class
            where TBody : class
            where TResponseError : BadRequestErroModel
        {
            return await this.RequestAsync<TResponse, TBody, TResponseError>(HttpMethod.Put, endPoint, action, queryParameters, body, logger, useAuthorization, useUserToken).ConfigureAwait(false);
        }
    }
}

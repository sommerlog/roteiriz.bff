﻿using Microsoft.Extensions.Logging;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Base;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client
{
    public class TipoVeiculoClient : BaseClient, ITipoVeiculoClient
    {

        public TipoVeiculoClient(ApiClient client, ILogger<TipoVeiculoClient> logger)
            : base(ApiEndPointEnum.Roteirizador, client, logger)
        {
        }

        public async Task<ITipoVeiculoResponse> AtivarTipoVeiculo(IAtivaTipoVeiculoRequest model)
        {
            try
            {
                return await PostAsync<ITipoVeiculoResponse, IAtivaTipoVeiculoRequest, BadRequestErroModel>
                    ($"veiculo/tipo/ativar", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<ITipoVeiculoResponse> CadastrarTipoVeiculo(ICadastraTipoVeiculoRequest model)
        {
            try
            {
                return await PostAsync<ITipoVeiculoResponse, ICadastraTipoVeiculoRequest, BadRequestErroModel>
                    ($"veiculo/tipo", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<ITipoVeiculoResponse> EditarTipoVeiculo(IEditaTipoVeiculoRequest model)
        {
            try
            {
                return await PutAsync<ITipoVeiculoResponse,IEditaTipoVeiculoRequest, BadRequestErroModel>
                    ($"veiculo/tipo",model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<ITipoVeiculoResponse> InativarTipoVeiculo(int id, int IdUsuario)
        {
            try
            {
                return await DeleteAsync<ITipoVeiculoResponse, BadRequestErroModel>
                    ($"veiculo/tipo/{id}?idUsuario={IdUsuario}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaTipoVeiculoResponse> ObterTipoVeiculoPorId(int id)
        {
            try
            {
                return await GetAsync<IConsultaTipoVeiculoResponse, BadRequestErroModel>
                    ($"veiculo/tipo/{id}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaTipoVeiculoResponse> ObterTipoVeiculos()
        {
            try
            {
                return await GetAsync<IConsultaTipoVeiculoResponse, BadRequestErroModel>
                    ($"veiculo/tipo", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }
    }
}

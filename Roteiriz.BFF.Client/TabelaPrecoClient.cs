﻿using Microsoft.Extensions.Logging;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Base;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client
{
    public class TabelaPrecoClient : BaseClient, ITabelaPrecoClient
    {

        public TabelaPrecoClient(ApiClient client, ILogger<TabelaPrecoClient> logger)
            : base(ApiEndPointEnum.Roteirizador, client, logger)
        {
        }

        public async Task<ITabelaPrecoResponse> AtivarTabelaPreco(IAtivaTabelaPrecoRequest model)
        {
            try
            {
                return await PostAsync<ITabelaPrecoResponse, IAtivaTabelaPrecoRequest, BadRequestErroModel>
                    ($"documento/planilha/importa", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<ITabelaPrecoResponse> CadastrarTabelaPreco(ICadastraTabelaPrecoRequest model)
        {
            try
            {
                return await PostAsync<ITabelaPrecoResponse, ICadastraTabelaPrecoRequest, BadRequestErroModel>
                    ($"tabela/preco", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<ITabelaPrecoResponse> EditarTabelaPreco(IEditaTabelaPrecoRequest model)
        {
            try
            {
                return await PutAsync<ITabelaPrecoResponse, IEditaTabelaPrecoRequest, BadRequestErroModel>
                    ($"tabela/preco", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<ITabelaPrecoResponse> InativarTabelaPreco(int id, int IdUsuario)
        {
            try
            {
                return await DeleteAsync<ITabelaPrecoResponse, BadRequestErroModel>
                    ($"tabela/preco/{id}?idUsuario={IdUsuario}",useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaTabelaPrecoResponse> ObterTabelaPrecoPorId(int id)
        {
            try
            {
                return await GetAsync<IConsultaTabelaPrecoResponse, BadRequestErroModel>
                    ($"tabela/preco/{id}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaTabelaPrecoResponse> ObterTabelaPrecos()
        {
            try
            {
                return await GetAsync<IConsultaTabelaPrecoResponse, BadRequestErroModel>
                    ($"tabela/preco", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }
    }
}

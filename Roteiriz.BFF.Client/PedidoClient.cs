﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Base;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client
{
    public class PedidoClient : BaseClient, IPedidoClient
    {

        public PedidoClient(ApiClient client, ILogger<PedidoClient> logger)
            : base(ApiEndPointEnum.Roteirizador, client, logger)
        {
        }

        public async Task<ImportaPlanilhaResponse> ImportarPlanilhaPedidos(ImportaPlanilhaRequest file)
        {
            try
            {
                return await PostAsync<ImportaPlanilhaResponse, ImportaPlanilhaRequest, BadRequestErroModel>
                    ($"documento/planilha/importa",file, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }
        public async Task<ImportaPlanilhaResponse> ObterPlanilhas(DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                return await GetAsync<ImportaPlanilhaResponse, BadRequestErroModel>
                    ($"documento/planilhas?DataInicio={dataInicio.ToString("yyyy-MM-dd")}&DataFim={dataFim.ToString("yyyy-MM-dd")}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IDetalheRotaResponse> ObterDetalheRota(IDetalheRotaRequest model)
        {
            try
            {
                return await PostAsync<IDetalheRotaResponse, IDetalheRotaRequest, BadRequestErroModel>
                    ($"rota/detalhes", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }


        public async Task<ConferirPedidoResponse> ConferirPedido(ConferirPedidoRequest model)
        {
            try
            {
                return await PostAsync<ConferirPedidoResponse, ConferirPedidoRequest, BadRequestErroModel>
                    ($"Pedido/conferir", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaEnderecoPedidoResponse> ObterEnderecosVeiculosPedido(string dataFim, string dataInicio)
        {
            try
            {
                return await GetAsync<IConsultaEnderecoPedidoResponse, BadRequestErroModel>
                    ($"Pedido/enderecos?dataInicio={dataInicio}&dataFim={dataFim}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaPedidoResponse> ObterPedidos(DateTime dataInicio, DateTime dataFim)
        {
            try
            {
                return await GetAsync<IConsultaPedidoResponse, BadRequestErroModel>
                    ($"Pedido?dataInicio={dataInicio.ToString("yyyy-MM-dd")}&dataFim={dataFim.ToString("yyyy-MM-dd")}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }


        public async Task<ConsultaRotaResponse> ConsultaRota(int idStatusRota, int idRota)
        {
            try
            {

                string url = $"rota?idStatusRota={idStatusRota}";

                if (idRota > 0)
                {
                    url = $"rota?idRota={idRota}";
                }
                
                return await GetAsync<ConsultaRotaResponse, BadRequestErroModel>
                    (url, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<CadastraRotaResponse> CadastrarRota(CadastraRotaRequest model)
        {
            try
            {
                return await PostAsync<CadastraRotaResponse, CadastraRotaRequest, BadRequestErroModel>
                    ($"rota",model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<AprovaReprovaRotaResponse> AprovarRota(AprovaReprovaRotaRequest model)
        {
            try
            {
                return await PostAsync<AprovaReprovaRotaResponse, AprovaReprovaRotaRequest, BadRequestErroModel>
                    ($"rota/Aprovar", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<AprovaReprovaRotaResponse> ReprovarRota(AprovaReprovaRotaRequest model)
        {
            try
            {
                return await PostAsync<AprovaReprovaRotaResponse, AprovaReprovaRotaRequest, BadRequestErroModel>
                    ($"rota/Reprovar", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<AprovaReprovaRotaResponse> ConfirmaSaidaRota(AprovaReprovaRotaRequest model)
        {
            try
            {
                return await PostAsync<AprovaReprovaRotaResponse, AprovaReprovaRotaRequest, BadRequestErroModel>
                    ($"rota/Saida", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<AprovaReprovaRotaResponse> FinalizarRota(AprovaReprovaRotaRequest model)
        {
            try
            {
                return await PostAsync<AprovaReprovaRotaResponse, AprovaReprovaRotaRequest, BadRequestErroModel>
                    ($"rota/Finalizar", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

    }
}

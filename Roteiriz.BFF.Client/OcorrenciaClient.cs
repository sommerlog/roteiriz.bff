﻿using Microsoft.Extensions.Logging;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Base;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Data.Models.Roteiriz.Model.Motorista;
using Roteiriz.Data.Models.Roteiriz.Model.Veiculo;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client
{
    public class OcorrenciaClient : BaseClient, IOcorrenciaClient
    {

        public OcorrenciaClient(ApiClient client, ILogger<OcorrenciaClient> logger)
            : base(ApiEndPointEnum.Roteirizador, client, logger)
        {
        }

        public async Task<OcorrenciaResponse> CadastrarOcorrencia(OcorrenciaRequest model)
        {
            try
            {
                return await PostAsync<OcorrenciaResponse, OcorrenciaRequest, BadRequestErroModel>
                    ($"Ocorrencia", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<OcorrenciaResponse> EditarOcorrencia(OcorrenciaRequest model)
        {
            try
            {
                return await PutAsync<OcorrenciaResponse, OcorrenciaRequest, BadRequestErroModel>
                    ($"Ocorrencia", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<OcorrenciaResponse> ObterOcorrencia(int id)
        {
            try
            {
                return await GetAsync<OcorrenciaResponse, BadRequestErroModel>
                    ($"Ocorrencia/{id}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<OcorrenciaResponse> ObterOcorrencias()
        {
            try
            {
                return await GetAsync<OcorrenciaResponse, BadRequestErroModel>
                    ($"Ocorrencia", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }
    }
}

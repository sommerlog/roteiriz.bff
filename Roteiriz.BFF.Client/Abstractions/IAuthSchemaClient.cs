﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client.Abstractions
{
    public interface IAuthSchemaClient
    {
        Task<LoginResponseModel> LoginUsuario(LoginRequestModel model);
        Task<IUserResponse> NovaSenhaUsuario(NewUserPasswordRequest model);
        Task<IUserResponse> consultaUsuario(string email);
        Task<IUserResponse> newUser(NewUserRequest model);
        Task<IUserResponse> AccessUser(NewUserAccessKeyRequest model);
        Task<IUserResponse> EditAccessUser(EditUserAccessKeyRequest model);
        


    }
}

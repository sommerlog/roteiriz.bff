﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client.Abstractions
{
    public interface IMotoristaClient
    {
        Task<IConsultaVeiculoMotoristaResponse> ObterVeiculosMotorista(int id);
        Task<IConsultaVeiculoMotoristaResponse> DeletarVeiculoMotorista(int idMotorista, int idVeiculo);

        Task<IConsultaVeiculoMotoristaResponse> AtribuirVeiculoMotorista(ICadastraVeiculoMotoristaRequest model);

        Task<IConsultaVeiculoMotoristaResponse> AlterarVeiculoPadraoMotorista(ICadastraVeiculoMotoristaRequest model);

        Task<IConsultaMotoristaResponse> ObterMotoristas();

        Task<IConsultaMotoristaResponse> ObterMotoristaPorId(int id);

        Task<IMotoristaResponse> CadastrarMotorista(ICadastraMotoristaRequest model);

        Task<IMotoristaResponse> EditarMotorista(IEditaMotoristaRequest model);

        Task<IMotoristaResponse> InativarMotorista(int idMotorista, int idUsuario);

        Task<IMotoristaResponse> AtivarMotorista(IAtivaMotoristaRequest model);
    }
}

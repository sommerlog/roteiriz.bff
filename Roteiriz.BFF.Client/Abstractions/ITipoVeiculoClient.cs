﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client.Abstractions
{
    public interface ITipoVeiculoClient
    {
        Task<IConsultaTipoVeiculoResponse> ObterTipoVeiculos();
        Task<IConsultaTipoVeiculoResponse> ObterTipoVeiculoPorId(int id);
        Task<ITipoVeiculoResponse> CadastrarTipoVeiculo(ICadastraTipoVeiculoRequest model);
        Task<ITipoVeiculoResponse> EditarTipoVeiculo(IEditaTipoVeiculoRequest model);
        Task<ITipoVeiculoResponse> InativarTipoVeiculo(int id, int IdUSuario);
        Task<ITipoVeiculoResponse> AtivarTipoVeiculo(IAtivaTipoVeiculoRequest model);
    }
}

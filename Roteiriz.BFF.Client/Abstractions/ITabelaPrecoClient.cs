﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client.Abstractions
{
    public interface ITabelaPrecoClient
    {
        Task<IConsultaTabelaPrecoResponse> ObterTabelaPrecos();
        Task<IConsultaTabelaPrecoResponse> ObterTabelaPrecoPorId(int id);
        Task<ITabelaPrecoResponse> CadastrarTabelaPreco(ICadastraTabelaPrecoRequest model);
        Task<ITabelaPrecoResponse> EditarTabelaPreco(IEditaTabelaPrecoRequest model);
        Task<ITabelaPrecoResponse> InativarTabelaPreco(int id, int IdUsuario);
        Task<ITabelaPrecoResponse> AtivarTabelaPreco(IAtivaTabelaPrecoRequest model);
    }
}

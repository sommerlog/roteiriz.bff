﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client.Abstractions
{
    public interface IVeiculoClient
    {
        Task<IConsultaVeiculoResponse> ObterVeiculos();
        Task<IConsultaVeiculoResponse> ObterVeiculoPorId(string placa, int IdUsuario);
        Task<IVeiculoResponse> CadastrarVeiculo(ICadastraVeiculoRequest model);
        Task<IVeiculoResponse> EditarVeiculo(IEditaVeiculoRequest model);
        Task<IVeiculoResponse> InativarVeiculo(string placa, int idUsuario);
        Task<IVeiculoResponse> AtivarVeiculo(IAtivaVeiculoRequest model);

    }
}

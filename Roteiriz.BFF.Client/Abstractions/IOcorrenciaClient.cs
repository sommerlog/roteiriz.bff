﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client.Abstractions
{
    public interface IOcorrenciaClient
    {
        Task<OcorrenciaResponse> ObterOcorrencias();
        Task<OcorrenciaResponse> ObterOcorrencia(int id);
        Task<OcorrenciaResponse> CadastrarOcorrencia(OcorrenciaRequest model);
        Task<OcorrenciaResponse> EditarOcorrencia(OcorrenciaRequest model);
    }
}

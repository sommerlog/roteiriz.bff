﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client.Abstractions
{
    public interface IGerenciadorClient
    {
        Task<IClienteResponse> consultaClientes();

        Task<IClienteResponse> cadastraClientes(IClienteRequest model);

        Task<IClienteResponse> editaCliente(IClienteRequest model);

        Task<IUserClienteResponse> consultaUsuarioCliente(int id);

        Task<IUserResponse> consultaUsuarios();

        Task<IDetalheUsuarioResponse> consultaDetalheUsuario(int id);

        Task<IProdutoResponse> consultaProdutos();

        Task<IUserClienteResponse> cadastraClienteUsuario(IClienteUsuarioRequest model);

        Task<IUserClienteResponse> editaClienteUsuario(IClienteUsuarioRequest model);
    }   
}



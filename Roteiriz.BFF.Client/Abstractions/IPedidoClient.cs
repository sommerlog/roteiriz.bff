﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client.Abstractions
{
    public interface IPedidoClient
    {
        Task<IConsultaPedidoResponse> ObterPedidos(DateTime dataInicio, DateTime dataFim);
        Task<IConsultaEnderecoPedidoResponse> ObterEnderecosVeiculosPedido(string dataFim, string dataInicio);
        Task<ConferirPedidoResponse> ConferirPedido(ConferirPedidoRequest model);


        Task<IDetalheRotaResponse> ObterDetalheRota(IDetalheRotaRequest model);
        Task<ImportaPlanilhaResponse> ImportarPlanilhaPedidos(ImportaPlanilhaRequest file);
        Task<ImportaPlanilhaResponse> ObterPlanilhas(DateTime dataInicio, DateTime dataFim);

        Task<ConsultaRotaResponse> ConsultaRota(int idStatusRota, int idRota);
        Task<CadastraRotaResponse> CadastrarRota(CadastraRotaRequest model);

        Task<AprovaReprovaRotaResponse> AprovarRota(AprovaReprovaRotaRequest model);
        Task<AprovaReprovaRotaResponse> ReprovarRota(AprovaReprovaRotaRequest model);
        Task<AprovaReprovaRotaResponse> ConfirmaSaidaRota(AprovaReprovaRotaRequest model);
        Task<AprovaReprovaRotaResponse> FinalizarRota(AprovaReprovaRotaRequest model);

    }
}

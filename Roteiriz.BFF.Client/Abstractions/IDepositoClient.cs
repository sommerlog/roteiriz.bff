﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client.Abstractions
{
    public interface IDepositoClient
    {
        Task<DepositoResponse> ObterDepositos();
        Task<DepositoResponse> ObterDeposito(int id);
        Task<DepositoResponse> CadastrarDeposito(DepositoRequest model);
        Task<DepositoResponse> EditarDeposito(DepositoRequest model);
        Task<TrocaDepositoUsuarioResponse> TrocarDepositoUsuario(TrocarUsuarioDepositoRequest model);
    }
}

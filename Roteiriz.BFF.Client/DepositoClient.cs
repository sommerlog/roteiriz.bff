﻿using Microsoft.Extensions.Logging;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Base;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Data.Models.Roteiriz.Model.Motorista;
using Roteiriz.Data.Models.Roteiriz.Model.Veiculo;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client
{
    public class DepositoClient : BaseClient, IDepositoClient
    {

        public DepositoClient(ApiClient client, ILogger<DepositoClient> logger)
            : base(ApiEndPointEnum.Roteirizador, client, logger)
        {
        }

        public async Task<DepositoResponse> CadastrarDeposito(DepositoRequest model)
        {
            try
            {
                return await PostAsync<DepositoResponse, DepositoRequest, BadRequestErroModel>
                    ($"deposito", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<DepositoResponse> EditarDeposito(DepositoRequest model)
        {
            try
            {
                return await PutAsync<DepositoResponse, DepositoRequest, BadRequestErroModel>
                    ($"deposito", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<DepositoResponse> ObterDeposito(int id)
        {
            try
            {
                return await GetAsync<DepositoResponse, BadRequestErroModel>
                    ($"deposito/{id}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<DepositoResponse> ObterDepositos()
        {
            try
            {
                return await GetAsync<DepositoResponse, BadRequestErroModel>
                    ($"deposito", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<TrocaDepositoUsuarioResponse> TrocarDepositoUsuario(TrocarUsuarioDepositoRequest model)
        {
            try
            {
                return await PutAsync<TrocaDepositoUsuarioResponse, TrocarUsuarioDepositoRequest, BadRequestErroModel>
                    ($"deposito/usuario",model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }
    }
}

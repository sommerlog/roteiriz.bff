﻿using Microsoft.Extensions.Logging;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Base;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Data.Models.Roteiriz.Model.Motorista;
using Roteiriz.Data.Models.Roteiriz.Model.Veiculo;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client
{
    public class MotoristaClient : BaseClient, IMotoristaClient
    {

        public MotoristaClient(ApiClient client, ILogger<MotoristaClient> logger)
            : base(ApiEndPointEnum.Roteirizador, client, logger)
        {
        }

        public async Task<IConsultaVeiculoMotoristaResponse> AlterarVeiculoPadraoMotorista(ICadastraVeiculoMotoristaRequest model)
        {
            try
            {
                return await PostAsync<IConsultaVeiculoMotoristaResponse, ICadastraVeiculoMotoristaRequest, BadRequestErroModel>
                    ($"motorista/veiculo/padrao", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IMotoristaResponse> AtivarMotorista(IAtivaMotoristaRequest model)
        {
            try
            {
                return await DeleteAsync<IMotoristaResponse, BadRequestErroModel>
                    ($"motorista/{model.IdMotorista}?idUsuario={model.IdUsuario}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaVeiculoMotoristaResponse> AtribuirVeiculoMotorista(ICadastraVeiculoMotoristaRequest model)
        {
            try
            {
                return await PostAsync<IConsultaVeiculoMotoristaResponse, ICadastraVeiculoMotoristaRequest, BadRequestErroModel>
                    ($"motorista/veiculo", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IMotoristaResponse> CadastrarMotorista(ICadastraMotoristaRequest model)
        {
            try
            {
                return await PostAsync<IMotoristaResponse, ICadastraMotoristaRequest, BadRequestErroModel>
                    ($"motorista", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaVeiculoMotoristaResponse> DeletarVeiculoMotorista(int idMotorista, int idVeiculo)
        {
            try
            {
                return await DeleteAsync<IConsultaVeiculoMotoristaResponse, BadRequestErroModel>
                    ($"motorista/veiculo?idMotorista={idMotorista}&idVeiculo={idVeiculo}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IMotoristaResponse> EditarMotorista(IEditaMotoristaRequest model)
        {
            try
            {
                return await PutAsync<IMotoristaResponse, IEditaMotoristaRequest, BadRequestErroModel>
                    ($"motorista", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IMotoristaResponse> InativarMotorista(int idMotorista, int idUsuario)
        {
            try
            {
                return await DeleteAsync<IMotoristaResponse, BadRequestErroModel>
                    ($"motorista/{idMotorista}?idUsuario={idUsuario}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaMotoristaResponse> ObterMotoristaPorId(int id)
        {
            try
            {
                return await GetAsync<IConsultaMotoristaResponse, BadRequestErroModel>
                    ($"motorista/{id}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaMotoristaResponse> ObterMotoristas()
        {
            try
            {
                return await GetAsync<IConsultaMotoristaResponse, BadRequestErroModel>
                    ($"motorista", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaVeiculoMotoristaResponse> ObterVeiculosMotorista(int id)
        {
            try
            {
                return await GetAsync<IConsultaVeiculoMotoristaResponse, BadRequestErroModel>
                    ($"motorista/veiculo/{id}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }
    }
}

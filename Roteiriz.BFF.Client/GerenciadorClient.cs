﻿using Microsoft.Extensions.Logging;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Base;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Data.Models.Roteiriz.Model.Usuario;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client
{
    public class GerenciadorClient : BaseClient, IGerenciadorClient 
    {
        public GerenciadorClient(ApiClient client, ILogger<GerenciadorClient> logger)
            : base(ApiEndPointEnum.Gerenciador, client, logger)
        {
            
        }

        public async Task<IClienteResponse> cadastraClientes(IClienteRequest model)
        {
            try
            {
                return await PostAsync<IClienteResponse, IClienteRequest, BadRequestErroModel>
                    ($"cliente", model, useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IUserClienteResponse> cadastraClienteUsuario(IClienteUsuarioRequest model)
        {

            try
            {
                return await PostAsync<IUserClienteResponse, IClienteUsuarioRequest, BadRequestErroModel>
                    ($"usuario", model, useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IClienteResponse> consultaClientes()
        {

            try
            {
                return await GetAsync<IClienteResponse, BadRequestErroModel>
                    ($"cliente" , useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IDetalheUsuarioResponse> consultaDetalheUsuario(int id)
        {
            try
            {
                return await GetAsync<IDetalheUsuarioResponse, BadRequestErroModel>
                    ($"usuario/detalhe/{id}", useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IProdutoResponse> consultaProdutos()
        {
            try
            {
                return await GetAsync<IProdutoResponse, BadRequestErroModel>
                    ($"cliente/produtos", useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }
        public async Task<IUserClienteResponse> consultaUsuarioCliente(int id)
        {
            try
            {
                return await GetAsync<IUserClienteResponse, BadRequestErroModel>
                    ($"usuario/cliente/{id}", useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public  async Task<IUserResponse> consultaUsuarios()
        {
            try
            {
                return await GetAsync<IUserResponse, BadRequestErroModel>
                    ($"usuario", useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IClienteResponse> editaCliente(IClienteRequest model)
        {
            try
            {
                return await PostAsync<IClienteResponse, IClienteRequest, BadRequestErroModel>
                    ($"cliente", model, useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async  Task<IUserClienteResponse> editaClienteUsuario(IClienteUsuarioRequest model)
        {
            try
            {
                return await PutAsync<IUserClienteResponse, IClienteUsuarioRequest, BadRequestErroModel>
                    ($"usuario", model, useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }
    }
}

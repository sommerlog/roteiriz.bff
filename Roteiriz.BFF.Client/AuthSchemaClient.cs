﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Base;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client
{
    public class AuthSchemaClient : BaseClient, IAuthSchemaClient
    {

        public AuthSchemaClient(ApiClient client, ILogger<AuthSchemaClient> logger)
            : base(ApiEndPointEnum.AuthSchema, client, logger)
        {
        }

        public async Task<IUserResponse> AccessUser(NewUserAccessKeyRequest model)
        {
            try
            {
                return await PostAsync<IUserResponse, NovoAccessKeyRequest, BadRequestErroModel>
                    ($"user/acesso", new NovoAccessKeyRequest(model), useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IUserResponse> consultaUsuario(string email)
        {
            try
            {
                var query = new QueryString(email);

                return await GetAsync<IUserResponse, BadRequestErroModel>
                    ($"user", query, useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IUserResponse> EditAccessUser(EditUserAccessKeyRequest model)
        {
            try
            {
                return await PutAsync<IUserResponse, EditarAccessKeyRequest, BadRequestErroModel>
                    ($"user/acesso", new EditarAccessKeyRequest(model), useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<LoginResponseModel> LoginUsuario(LoginRequestModel model)
        {
            try
            {
                return await PostAsync<LoginResponseModel,LoginRequest, BadRequestErroModel>
                    ($"user/login",new LoginRequest(model.Email, model.Password, 3),useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }catch(BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IUserResponse> newUser(NewUserRequest model)
        {
            try
            {
                return await PostAsync<IUserResponse, NewUserRequest, BadRequestErroModel>
                    ($"user", model, useAuthorization: false, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IUserResponse> NovaSenhaUsuario(NewUserPasswordRequest model)
        {
            try
            {
                return await PostAsync<IUserResponse, PasswordRequest, BadRequestErroModel>
                    ($"user/senha", new PasswordRequest(model.userId, model.productId, model.newpassword),useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }
    }
}

﻿using Microsoft.Extensions.Logging;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Base;
using Roteiriz.Data.Enums;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Client
{
    public class VeiculoClient : BaseClient, IVeiculoClient
    {

        public VeiculoClient(ApiClient client, ILogger<VeiculoClient> logger)
            : base(ApiEndPointEnum.Roteirizador, client, logger)
        {
        }

        public async Task<IVeiculoResponse> AtivarVeiculo(IAtivaVeiculoRequest model)
        {
            try
            {
                return await PostAsync<IVeiculoResponse, IAtivaVeiculoRequest, BadRequestErroModel>
                    ($"veiculo/ativar", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IVeiculoResponse> CadastrarVeiculo(ICadastraVeiculoRequest model)
        {
            try
            {
                return await PostAsync<IVeiculoResponse, ICadastraVeiculoRequest, BadRequestErroModel>
                    ($"veiculo", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IVeiculoResponse> EditarVeiculo(IEditaVeiculoRequest model)
        {
            try
            {
                return await PutAsync<IVeiculoResponse, IEditaVeiculoRequest, BadRequestErroModel>
                    ($"veiculo", model, useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IVeiculoResponse> InativarVeiculo(string placa, int idUsuario)
        {
            try
            {
                return await DeleteAsync<IVeiculoResponse, BadRequestErroModel>
                    ($"veiculo/{placa}?idUsuario={idUsuario}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }


        /// TODO METODO ERRADO
        public async Task<IConsultaVeiculoResponse> ObterVeiculoPorId(string placa, int IdUsuario)
        {
            try
            {
                return await DeleteAsync<IConsultaVeiculoResponse, BadRequestErroModel>
                    ($"veiculo/{placa}?idUsuario={IdUsuario}", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }

        public async Task<IConsultaVeiculoResponse> ObterVeiculos()
        {
            try
            {
                return await GetAsync<IConsultaVeiculoResponse, BadRequestErroModel>
                    ($"veiculo", useAuthorization: true, useUserToken: true).ConfigureAwait(false);
            }
            catch (BaseApiException<BadRequestErroModel> ex)
            {
                throw ex;
            }
        }
    }
}

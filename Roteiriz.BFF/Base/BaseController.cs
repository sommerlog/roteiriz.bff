﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Roteiriz.Data.Exceptions;
using Roteiriz.Data.Models;
using Roteiriz.Data.Models.Roteiriz.Error;
using Roteiriz.Services;
using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Roteiriz.BFF.Base
{
    public class BaseController : Controller
    {

        public TokenService TokenService { get; set; }


        public BaseController(TokenService tokenService)
        {
            TokenService = tokenService;
        }

        protected async Task<IActionResult> ResponseApi(Func<Task> function)
        {
            return await ResponseApi<object>(async () => { await function().ConfigureAwait(false); return null; }).ConfigureAwait(false);
        }

        protected async Task<IActionResult> ResponseApi<T>(Func<T> function)
        {
            return await ResponseApi(async () => Task.Run(function).ConfigureAwait(false)).ConfigureAwait(false);
        }

        protected async Task<IActionResult> ResponseApi<T>(Func<Task<T>> function)
        {
            var stopwatch = new Stopwatch();

            try
            {

                stopwatch.Start();
                
                var content = await function().ConfigureAwait(false);

                return Ok(content);
            }
            catch (Exception ex)
            {

                var type = ex.GetType();
                if (type.Name == typeof(BaseApiException<BadRequestErroModel>).Name)
                {
                    object response = new { };
                    if (type.IsGenericType)
                    {
                        response = type.GetProperty("Response").GetValue(ex);
                    }

                    return StatusCode(400, response);
                }
                else
                {
                    var message = ex.Message;
                    if(ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                    {
                        message += " - " + ex.InnerException.Message;
                    }


                    if (string.IsNullOrEmpty(message))
                        message = "Erro crítico ao enviar a solicitação";


                    var response  = new BadRequestErroModel()
                    {
                        Controle = new Data.Models.Roteiriz.Response.ControleResponse()
                        {
                            Message = message,
                            Prefix  = "mid.exc.err.001"
                        },
                        Error = new BadRequestErroModel.ErrorResponse
                        {
                            Message = ex?.InnerException?.Message
                        }
                        
                    };

                    return StatusCode(500, response);
                }
            }
            finally
            {
                stopwatch.Stop();
            }
        }
    }
}

﻿using Roteiriz.Data.Enums;
using System.Net;

namespace Roteiriz.BFF.Base
{
    public class BaseApiException : Exception
    {
        public StatusCodeEnum StatusCode { get;private set; }
        public HttpStatusCode HttpStatusCode { get;private set; }

        public BaseApiException(StatusCodeEnum statusCodes, HttpStatusCode httpStatusCode) : base()
        {
            StatusCode = statusCodes;
            HttpStatusCode = httpStatusCode;
        }

        public BaseApiException(StatusCodeEnum statusCodes, HttpStatusCode httpStatusCode, string message) : base(message)
        {
            StatusCode = statusCodes;
            HttpStatusCode = httpStatusCode;
        }
    }

    public class BaseApiException<TResponse> : BaseApiException
        where TResponse : class
    {
        public TResponse Response { get; private set; }

        public BaseApiException(StatusCodeEnum statusCodes, HttpStatusCode httpStatusCode,TResponse response) : base(statusCodes, httpStatusCode)
        {
            Response = response;
        }

        public BaseApiException(StatusCodeEnum statusCodes, HttpStatusCode httpStatusCode, TResponse response, string message) : base(statusCodes, httpStatusCode, message)
        {
            Response = response;
        }
    }
}

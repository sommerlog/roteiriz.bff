using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Roteiriz.BFF.Client;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Models;
using Roteiriz.Data.Models.Config;
using Roteiriz.Services;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;
using System.Runtime.CompilerServices;
using Roteiriz.BFF.HostService;

namespace Roteiriz.BFF
{

    public class Program
    {
        public static void Main(string[] args)
        {

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, true)
                .AddEnvironmentVariables()
                .Build();


            var GoUpAsAService = config.GetValue<bool>("GoUpAsAService");

            if (GoUpAsAService)
            {
                var isService = !(Debugger.IsAttached || args.Contains("--console"));

                var pathToContentRoot = Directory.GetCurrentDirectory();

                if (isService)
                {
                    var pathToExe = Process.GetCurrentProcess().MainModule.FileName;
                    pathToContentRoot = Path.GetDirectoryName(pathToExe);
                }

                var build = WebHost.CreateDefaultBuilder(args)
                    .UseContentRoot(pathToContentRoot)
                    .ConfigureAppConfiguration(cfg =>
                    {
                        cfg.SetBasePath(pathToContentRoot);
                        cfg.AddJsonFile("appsettings.json", false, true);
                    })
                    .UseStartup<Startup>()
                    .UseIISIntegration()
                    .UseKestrel(opt =>
                    {
                        opt.ConfigureEndpoints();
                    })
                    .Build();

                if (isService)
                    build.RunAsACustomService();
                else
                    build.Run();
            }
            else
            {
                BuildWebHost(args).Run();
            }


        }
        
        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((hostingContext, config) =>
            {
                var env = hostingContext.HostingEnvironment;
                config.AddCommandLine(args);
                config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                config.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true);
                config.AddEnvironmentVariables();
            })
            .UseKestrel(_ => _.AddServerHeader = false)
            .UseContentRoot(Directory.GetCurrentDirectory())
            .UseIISIntegration()
            .UseStartup<Startup>()
            .Build();
    }

}

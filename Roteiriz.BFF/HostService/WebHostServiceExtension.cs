﻿using Microsoft.AspNetCore.Hosting;
using System.ServiceProcess;

namespace Roteiriz.BFF
{
    public static class WebHostServiceExtension
    {
        public static void RunAsACustomService(this IWebHost host)
        {
            var webHostService = new CustomWebHostService(host);

            ServiceBase.Run(webHostService);
        }
    }
}

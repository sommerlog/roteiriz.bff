﻿using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Writers;
using Roteiriz.BFF.Base;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Services;
using System.Reflection;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Controllers
{
    [ApiController]
    [Route("api/roteirizador/motorista")]
    public class MotoristaController : BaseController
    {

        public MotoristaController(TokenService token) : base(token)
        {

        }


        [HttpGet("veiculo/{id:int}")]
        public async Task<IActionResult> ObterVeiculo(
            [FromRoute] int id,
            [FromServices] IMotoristaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterVeiculosMotorista(id)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpDelete("veiculo")]
        public async Task<IActionResult> DeletarVeiculoMotorista(
            [FromQuery] int idMotorista, 
            [FromQuery] int idVeiculo,
            [FromServices] IMotoristaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.DeletarVeiculoMotorista(idMotorista, idVeiculo)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("veiculo")]
        ///IConsultaVeiculoMotoristaResponse
        public async Task<IActionResult> AtribuirVeiculoMotorista(
            [FromBody] ICadastraVeiculoMotoristaRequest model,
            [FromServices] IMotoristaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.AtribuirVeiculoMotorista(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("veiculo/padrao")]
        ///IConsultaVeiculoMotoristaResponse
        public async Task<IActionResult> AlterarVeiculoPadraoMotorista(
            [FromBody] ICadastraVeiculoMotoristaRequest model,
            [FromServices] IMotoristaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.AlterarVeiculoPadraoMotorista(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet]
        ///IConsultaMotoristaResponse
        public async Task<IActionResult> ObterMotoristas(
            [FromServices] IMotoristaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterMotoristas()
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("{id:int}")]
        //IConsultaMotoristaResponse
        public async Task<IActionResult> ObterMotoristaPorId(
            [FromRoute] int id,
            [FromServices] IMotoristaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterMotoristaPorId(id)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost]
        //IMotoristaResponse
        public async Task<IActionResult> CadastrarMotorista(
            [FromBody]ICadastraMotoristaRequest model,
            [FromServices] IMotoristaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.CadastrarMotorista(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPut]
        //IMotoristaResponse
        public async Task<IActionResult> EditarMotorista(
            [FromBody] IEditaMotoristaRequest model,
            [FromServices] IMotoristaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.EditarMotorista(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpDelete]
        [Route("{id:int}")]
        //IMotoristaResponse
        public async Task<IActionResult> InativarMotorista(
            [FromRoute] int id, 
            [FromQuery] int idUsuario,
            [FromServices] IMotoristaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.InativarMotorista(id,idUsuario)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost]
        [Route("ativar")]
        //IMotoristaResponse
        public async Task<IActionResult>  AtivarMotorista(
            [FromBody] IAtivaMotoristaRequest model,
            [FromServices] IMotoristaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.AtivarMotorista(model)
                    .ConfigureAwait(false);
                return response;
            });
        }


    }
}
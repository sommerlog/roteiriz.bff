﻿using Microsoft.AspNetCore.Mvc;
using Roteiriz.BFF.Base;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Services;

namespace Roteiriz.BFF.Controllers
{
    [ApiController]
    [Route("api/cardapioVirtual")]
    public class GerenciadorController: BaseController
    {
        public GerenciadorController(TokenService service) : base(service)
        {
            
        }

        [HttpGet("clientes")]
        public async Task<IActionResult> consultaClientes(
            [FromServices] IGerenciadorClient client
            )
        {
            return await ResponseApi(async () =>
            {
                var response = await client.consultaClientes()
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("cliente")]
        public async Task<IActionResult> cadastraClientes(
            [FromServices] IGerenciadorClient client,
            [FromBody] IClienteRequest model
            )
        {
            return await ResponseApi(async () =>
            {
                var response = await client.cadastraClientes(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPut("cliente")]
        public async Task<IActionResult> editaCliente(
            [FromServices] IGerenciadorClient client,
            [FromBody] IClienteRequest model
            )
        {
            return await ResponseApi(async () =>
            {
                var response = await client.editaCliente(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("cliente/{id:int}/usuarios")]
        public async Task<IActionResult> consultaUsuarioCliente(
            [FromServices] IGerenciadorClient client,
            [FromRoute] int id
            )
        {
            return await ResponseApi(async () =>
            {
                var response = await client.consultaUsuarioCliente(id)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("usuarios")]
        public async Task<IActionResult> consultaUsuarios(
            [FromServices] IGerenciadorClient client
            )
        {
            return await ResponseApi(async () =>
            {
                var response = await client.consultaUsuarios()
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("usuario/detalhe/{id:int}")]
        public async Task<IActionResult> consultaDetalheUsuario(
            [FromServices] IGerenciadorClient client,
            [FromRoute] int id
            )
        {
            return await ResponseApi(async () =>
            {
                var response = await client.consultaDetalheUsuario(id)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("produtos")]
        public async Task<IActionResult> consultaProdutos(
           [FromServices] IGerenciadorClient client
           )
        {
            return await ResponseApi(async () =>
            {
                var response = await client.consultaProdutos()
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("usuario/cliente")]
        public async Task<IActionResult> cadastraClienteUsuario(
          [FromServices] IGerenciadorClient client,
          [FromBody] IClienteUsuarioRequest model
          )
        {
            return await ResponseApi(async () =>
            {
                var response = await client.cadastraClienteUsuario(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPut("usuario/cliente")]
        public async Task<IActionResult> editaClienteUsuario(
         [FromServices] IGerenciadorClient client,
         [FromBody] IClienteUsuarioRequest model
         )
        {
            return await ResponseApi(async () =>
            {
                var response = await client.editaClienteUsuario(model)
                    .ConfigureAwait(false);
                return response;
            });
        }




    }

}

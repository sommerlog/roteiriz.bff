﻿using Microsoft.AspNetCore.Mvc;
using Roteiriz.BFF.Base;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Services;

namespace Roteiriz.BFF.Controllers
{
    [ApiController]
    [Route("api/roteirizador/ocorrencia")]
    public class OcorrenciaController : BaseController
    {
        public OcorrenciaController(TokenService token) : base(token)
        {

        }

        [HttpGet]
        public async Task<IActionResult> ObterOcorrencias(
            [FromServices] IOcorrenciaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterOcorrencias()
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> ObterOcorrencia(
            [FromRoute] int id,
            [FromServices] IOcorrenciaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterOcorrencia(id)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost]
        public async Task<IActionResult> CadastrarOcorrencia(
            [FromBody] OcorrenciaRequest model,
            [FromServices] IOcorrenciaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.CadastrarOcorrencia(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPut]
        public async Task<IActionResult> EditarOcorrencia(
            [FromBody] OcorrenciaRequest model,
            [FromServices] IOcorrenciaClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.EditarOcorrencia(model)
                    .ConfigureAwait(false);
                return response;
            });
        }
    }
}

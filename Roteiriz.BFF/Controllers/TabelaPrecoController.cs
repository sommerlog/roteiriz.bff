﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Reflection.PortableExecutable;
using System.Threading.Tasks;
using Roteiriz.BFF.Base;
using Roteiriz.Services;
using Roteiriz.BFF.Client.Abstractions;

namespace Roteiriz.BFF.Controllers
{
    [ApiController]
    [Route("api/roteirizador/tabela/preco")]
    public class TabelaPrecoController : BaseController
    {

        public TabelaPrecoController(TokenService token) : base(token)
        {

        }

        [HttpGet]
        public async Task<IActionResult> ObterTabelaPrecos(
            [FromServices] ITabelaPrecoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterTabelaPrecos()
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> ObterTabelaPrecoPorId(
            [FromRoute] int id,
            [FromServices] ITabelaPrecoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterTabelaPrecoPorId(id)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost]
        public async Task<IActionResult> CadastrarTabelaPreco(
            [FromBody] ICadastraTabelaPrecoRequest model,
            [FromServices] ITabelaPrecoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.CadastrarTabelaPreco(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPut]
        public async Task<IActionResult> EditarTabelaPreco(
            [FromBody] IEditaTabelaPrecoRequest model,
            [FromServices] ITabelaPrecoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.EditarTabelaPreco(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> InativarTabelaPreco(
            [FromRoute] int id, 
            [FromQuery] int idUsuario,
            [FromServices] ITabelaPrecoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.InativarTabelaPreco(id, idUsuario)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("ativar")]
        public async Task<IActionResult> AtivarTabelaPreco(
            [FromBody] IAtivaTabelaPrecoRequest model,
            [FromServices] ITabelaPrecoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.AtivarTabelaPreco(model)
                    .ConfigureAwait(false);
                return response;
            });
        }
    }
}

﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Roteiriz.BFF.Base;
using Roteiriz.Services;
using Roteiriz.BFF.Client.Abstractions;

namespace Roteiriz.BFF.Controllers
{
    [ApiController]
    [Route("api/roteirizador/veiculo")]
    public class VeiculoController : BaseController
    {

        public VeiculoController(TokenService token) : base(token)
        {

        }

        [HttpGet]
        public async Task<IActionResult> ObterVeiculos(
            [FromServices] IVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterVeiculos()
                    .ConfigureAwait(false);
                return response;
            }); 
        }

        [HttpGet("{placa}")]
        public async Task<IActionResult> ObterVeiculoPorId(
            [FromRoute] string placa,
            [FromQuery] int idUsuario,
            [FromServices] IVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterVeiculoPorId(placa, idUsuario)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost]
        public async Task<IActionResult> CadastrarVeiculo(
            [FromBody] ICadastraVeiculoRequest model,
            [FromServices] IVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.CadastrarVeiculo(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPut]
        public async Task<IActionResult> EditarVeiculo(
            [FromBody] IEditaVeiculoRequest model,
            [FromServices] IVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.EditarVeiculo(model)
                    .ConfigureAwait(false);
                return response;
            }); 
        }

        [HttpDelete("{placa}")]
        public async Task<IActionResult> InativarVeiculo(
            [FromRoute] string placa, 
            [FromQuery] int IdUsuario,
            [FromServices] IVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.InativarVeiculo(placa, IdUsuario)
                    .ConfigureAwait(false);
                return response;
            }); 
        }

        [HttpPost("ativar")]
        public async Task<IActionResult> AtivarVeiculo(
            [FromBody] IAtivaVeiculoRequest model,
            [FromServices] IVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.AtivarVeiculo(model)
                    .ConfigureAwait(false);
                return response;
            });
        }
    }
}

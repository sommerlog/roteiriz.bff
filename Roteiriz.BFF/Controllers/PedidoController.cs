﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;
using Roteiriz.BFF.Base;
using Roteiriz.Services;
using Roteiriz.BFF.Client.Abstractions;

namespace Roteiriz.BFF.Controllers
{

    [ApiController]
    [Route("api/roteirizador/pedido")]
    public class PedidoController : BaseController
    {

        public PedidoController(TokenService token) : base(token)
        {

        }

        [HttpGet]
        public async Task<IActionResult> ObterVeiculosPedido(
            [FromQuery] DateTime dataInicio, 
            [FromQuery] DateTime dataFim,
            [FromServices] IPedidoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterPedidos(dataInicio, dataFim)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("Enderecos")]
        public async Task<IActionResult> ObterEnderecosVeiculosPedido(
            [FromQuery] string dataInicio, 
            [FromQuery] string dataFim,
            [FromServices] IPedidoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterEnderecosVeiculosPedido(dataFim, dataInicio)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("Conferir")]
        public async Task<IActionResult> ConferirPedido(
            [FromBody] ConferirPedidoRequest model,
            [FromServices] IPedidoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ConferirPedido(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("Rota/Detalhes")]
        public async Task<IActionResult> ObterDetalheRota(
            [FromBody] IDetalheRotaRequest model,
            [FromServices] IPedidoClient client) 
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterDetalheRota(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("Rota")]
        public async Task<IActionResult> ConsultarRotas(
            [FromServices] IPedidoClient client,
            [FromQuery] int idStatusRota, 
            [FromQuery] int idrota)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ConsultaRota(idStatusRota, idrota)
                    .ConfigureAwait(false);
                return response;
            });
        }
        
        [HttpPost("Rota")]
        public async Task<IActionResult> CadastrarRota(
            [FromBody] CadastraRotaRequest model,
            [FromServices] IPedidoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.CadastrarRota(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("Rota/Aprovar")]
        public async Task<IActionResult> AprovarRota(
            [FromBody] AprovaReprovaRotaRequest model,
            [FromServices] IPedidoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.AprovarRota(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("Rota/Reprovar")]
        public async Task<IActionResult> ReprovarRota(
            [FromBody] AprovaReprovaRotaRequest model,
            [FromServices] IPedidoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ReprovarRota(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("Rota/Saida")]
        public async Task<IActionResult> ConfirmaSaidaRota(
            [FromBody] AprovaReprovaRotaRequest model,
            [FromServices] IPedidoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ConfirmaSaidaRota(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("Rota/Finalizar")]
        public async Task<IActionResult> FinalizarRota(
            [FromBody] AprovaReprovaRotaRequest model,
            [FromServices] IPedidoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.FinalizarRota(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost]
        [Route("Planilha/Importar")]
        public async Task<IActionResult> ImportarPlanilhaPedidos(
            [FromBody] ImportaPlanilhaRequest model,
            [FromServices] IPedidoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ImportarPlanilhaPedidos(model)
               .ConfigureAwait(false);

                return response;
            });
        }

        [HttpGet]
        [Route("Planilhas")]
        public async Task<IActionResult> ObterPlanilhas(
            [FromServices] IPedidoClient client,
            [FromQuery] DateTime dataInicio,
            [FromQuery] DateTime dataFim)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterPlanilhas(dataInicio, dataFim)
               .ConfigureAwait(false);

                return response;
            });
        }
    }
}


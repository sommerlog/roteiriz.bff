﻿using Microsoft.AspNetCore.Mvc;
using Roteiriz.BFF.Base;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Services;

namespace Roteiriz.BFF.Controllers
{
    [ApiController]
    [Route("api/roteirizador/deposito")]
    public class DepositoController : BaseController
    {
        public DepositoController(TokenService token) : base(token)
        {

        }

        [HttpGet]
        public async Task<IActionResult> ObterDepositos(
            [FromServices] IDepositoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterDepositos()
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> ObterDeposito(
            [FromRoute] int id,
            [FromServices] IDepositoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterDeposito(id)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost]
        public async Task<IActionResult> CadastrarDeposito(
            [FromBody] DepositoRequest model,
            [FromServices] IDepositoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.CadastrarDeposito(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPut]
        public async Task<IActionResult> EditarDeposito(
            [FromBody] DepositoRequest model,
            [FromServices] IDepositoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.EditarDeposito(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPut("usuario")]
        public async Task<IActionResult> TrocarDepositoUsuario(
            [FromBody] TrocarUsuarioDepositoRequest model,
            [FromServices] IDepositoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.TrocarDepositoUsuario(model)
                    .ConfigureAwait(false);
                return response;
            });
        }
    }
}

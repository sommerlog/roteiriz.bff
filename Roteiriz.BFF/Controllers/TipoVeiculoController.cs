﻿using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Data.Models.Roteiriz.Response;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Roteiriz.BFF.Base;
using Roteiriz.Services;
using Roteiriz.BFF.Client.Abstractions;

namespace Roteiriz.BFF.Controllers
{
    [ApiController]
    [Route("api/roteirizador/veiculo/tipo")]
    public class TipoVeiculoController : BaseController
    {

        public TipoVeiculoController(TokenService token) : base(token)
        {

        }

        [HttpGet]
        public async Task<IActionResult> ObterTipoVeiculos(
            [FromServices] ITipoVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterTipoVeiculos()
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpGet("{id:int}")]
        public async Task<IActionResult> ObterTipoVeiculoPorId(
            [FromRoute] int id,
            [FromServices] ITipoVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.ObterTipoVeiculoPorId(id)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost]
        public async Task<IActionResult> CadastrarTipoVeiculo(
            [FromBody] ICadastraTipoVeiculoRequest model,
            [FromServices] ITipoVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.CadastrarTipoVeiculo(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPut]
        public async Task<IActionResult> EditarTipoVeiculo(
            [FromBody] IEditaTipoVeiculoRequest model,
            [FromServices] ITipoVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.EditarTipoVeiculo(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpDelete("{id:int}")]
        public async Task<IActionResult> InativarTipoVeiculo(
            [FromRoute] int id, 
            [FromQuery] int idUsuario,
            [FromServices] ITipoVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.InativarTipoVeiculo(id, idUsuario)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("ativar")]
        public async Task<IActionResult> AtivarTipoVeiculo(
            [FromBody] IAtivaTipoVeiculoRequest model,
            [FromServices] ITipoVeiculoClient client)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.AtivarTipoVeiculo(model)
                    .ConfigureAwait(false);
                return response;
            });
        }
    }
}

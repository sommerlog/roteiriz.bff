﻿using Microsoft.AspNetCore.Mvc;
using Roteiriz.BFF.Base;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.Data.Models.Roteiriz.Request;
using Roteiriz.Services;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Roteiriz.BFF.Controllers
{
    [ApiController]
    [Route("api/authschema")]
    public class AuthschemaController : BaseController
    {

        public AuthschemaController(TokenService token) : base(token)
        {

        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(
            [FromServices] IAuthSchemaClient client,
            [FromBody] LoginRequestModel model)
        {
            return await ResponseApi(async () =>
            {

                var response = await client.LoginUsuario(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("usuario/novaSenha")]
        public async Task<IActionResult> NovaSenhaUsuario(
            [FromServices] IAuthSchemaClient client,
            [FromBody] NewUserPasswordRequest model)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.NovaSenhaUsuario(model)
                    .ConfigureAwait(false);

                return response;
            });

        }

        [HttpGet]
        public async Task<IActionResult> consultaUsuario(
            [FromServices] IAuthSchemaClient client,
            [FromQuery] string email)
        {
            return await ResponseApi(async () =>
            {
                var response = await client.consultaUsuario(email)
                    .ConfigureAwait(false);

                return response;
            });

        }

        [HttpPost("usuario")]
        public async Task<IActionResult> newUser(
        [FromServices] IAuthSchemaClient client,
        [FromBody] NewUserRequest model)

        {
            return await ResponseApi(async () =>
            {
                var response = await client.newUser(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPost("usuario/accessKey")]
        public async Task<IActionResult> AccessUser(
        [FromServices] IAuthSchemaClient client,
        [FromBody] NewUserAccessKeyRequest model)

        {
            return await ResponseApi(async () =>
            {
                var response = await client.AccessUser(model)
                    .ConfigureAwait(false);
                return response;
            });
        }

        [HttpPut("usuario/accessKey")]
        public async Task<IActionResult> EditAccessUser(
        [FromServices] IAuthSchemaClient client,
        [FromBody] EditUserAccessKeyRequest model)

        {
            return await ResponseApi(async () =>
            {
                var response = await client.EditAccessUser(model)
                    .ConfigureAwait(false);
                return response;
            });
        }



    }
}

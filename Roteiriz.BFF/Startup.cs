﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Roteiriz.BFF.Client.Abstractions;
using Roteiriz.BFF.Client;
using Roteiriz.Data.Models.Config;
using Roteiriz.Data.Models;
using Roteiriz.Services;
using System.Text;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Enyim.Caching;
using Enyim.Caching.Configuration;

namespace Roteiriz.BFF
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();

            services.Configure<ClientConfiguration>(Configuration.GetSection("ClientConfiguration"));
            services.Configure<TokenServiceConfig>(Configuration.GetSection("TokenServiceConfig"));
            services.Configure<AuthorizationConfig>(Configuration.GetSection("AuthorizationConfig"));
            services.AddSingleton(Configuration);

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<IUserContextService, UserContextService>();
            services.AddScoped<IMemcachedClientConfiguration, MemcachedClientConfiguration>();
            services.AddScoped<IMemcachedClient, MemcachedClient>();
            services.AddScoped<TokenService>();


            services.AddScoped<ApiClient>();
            services.AddScoped<IAuthSchemaClient, AuthSchemaClient>();
            services.AddScoped<IGerenciadorClient, GerenciadorClient>();
            services.AddScoped<IMotoristaClient, MotoristaClient>();
            services.AddScoped<IVeiculoClient, VeiculoClient>();
            services.AddScoped<ITipoVeiculoClient, TipoVeiculoClient>();
            services.AddScoped<ITabelaPrecoClient, TabelaPrecoClient>();
            services.AddScoped<IPedidoClient, PedidoClient>();
            services.AddScoped<IDepositoClient, DepositoClient>();
            services.AddScoped<IOcorrenciaClient, OcorrenciaClient>();

            services.AddCors();

            var key = Encoding.ASCII.GetBytes("fweonisovubqerobvjqdfbvçkqberivb");
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };

            });

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Roteiriz.BFF", Version = "v1" });
            });


        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Roteiriz.BFF v1"));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseCors(opt => opt.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

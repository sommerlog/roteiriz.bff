﻿using Roteiriz.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Interfaces.Services
{
    public interface IControleProvider
    {
        ControleRequestModel ControleRequest { get; }
    }
}

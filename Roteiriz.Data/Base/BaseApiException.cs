﻿using Roteiriz.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Base
{
    public class BaseApiException<TResponse> : Exception
    {
        public TResponse Response { get; private set; }

        public BaseApiException(TResponse response) : base()
        {
            Response = response;
        }

    }
}

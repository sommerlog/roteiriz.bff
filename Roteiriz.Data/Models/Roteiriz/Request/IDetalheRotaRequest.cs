﻿

using Roteiriz.Data.Models.Roteiriz.Model.Rota;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class IDetalheRotaRequest
    {
        public DestinoDeposito Origem { get; set; }
        public IEnumerable<Waypoint> Waypoints { get; set; }
        public DestinoDeposito DestinoDeposito { get; set; }
        public DestinoQualquer DestinoQualquer{ get; set; }
        public bool VoltaParaDeposito { get; set; }
    }
}

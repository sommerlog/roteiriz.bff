﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class DepositoRequest
    {
        public int IdDeposito { get; set; }
        public string Descricao { get; set; }
        public string Endereco { get; set; }
        public string? Latitude { get; set; }
        public string? Longitude { get; set; }
    }
}

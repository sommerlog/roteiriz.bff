﻿
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class ICadastraVeiculoMotoristaRequest
    {
        public int IdVeiculo { get; set; }
        public int IdMotorista { get; set; }
        public bool VeiculoPadrao { get; set; }
    }
}

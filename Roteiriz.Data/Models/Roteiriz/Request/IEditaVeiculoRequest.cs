﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class IEditaVeiculoRequest
    {
        public int IdVeiculo { get; set; }
        public int IdTipoVeiculo { get; set; }
        public string Placa { get; set; }
        public string NrDoc { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Versao { get; set; }
        public string AnoFabricacao { get; set; }
        public string AnoModelo { get; set; }
        public DateTime DataUltimoLicenciamento { get; set; }
        public DateTime DataPesquisa { get; set; }
        public string NumeroANTT { get; set; }
        public double Peso { get; set; }
        public double Valor { get; set; }
        public double Cubagem_Diametro { get; set; }
        public double Cubagem_Largura { get; set; }
        public double Cubagem_Altura { get; set; }
        public bool Ativo { get; set; }
        public int? UsuarioAlteracao { get; set; }
    }
}

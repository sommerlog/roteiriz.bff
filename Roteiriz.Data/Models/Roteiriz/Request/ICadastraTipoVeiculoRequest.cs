﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class ICadastraTipoVeiculoRequest
    {
        public string Categoria { get; set; }
        public string Icone { get; set; }
        public double Peso { get; set; }
        public double Valor { get; set; }
        public double Cubagem_Diametro { get; set; }
        public double Cubagem_Largura { get; set; }
        public double Cubagem_Altura { get; set; }
        public bool Ativo { get; set; }
        public int UsuarioCadastro { get; set; }
    }
}

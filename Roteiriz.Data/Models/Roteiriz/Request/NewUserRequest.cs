﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public  class NewUserRequest
    {
        public string nomeUsuario { get; set; }
        public string email { get; set; }

    }

}

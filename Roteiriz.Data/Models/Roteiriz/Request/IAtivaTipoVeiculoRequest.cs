﻿

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class IAtivaTipoVeiculoRequest
    {
        public int IdTipoVeiculo { get; set; }
        public int IdUsuario { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class ICadastraTabelaPrecoRequest
    {
        public string CEPInicio { get; set; }
        public string CEPFinal { get; set; }
        public double Valor { get; set; }
        public int IdTipoVeiculo { get; set; }
        public bool Ativo { get; set; }
        public int UsuarioCadastro { get; set; }
    }
}

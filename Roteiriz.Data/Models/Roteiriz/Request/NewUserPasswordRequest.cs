﻿using Roteiriz.Data.Models.Roteiriz.Model.Identificador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class NewUserPasswordRequest
    {
        public int userId { get; set; }
        public int productId { get; set; }
        public string newpassword { get; set; }
        public string token { get; set; }
    }

    public class PasswordRequest
    {
        public PasswordRequest(int idUsuario, int idProduto, string senha)
        {
            Usuario = new Identificador { _Id = idUsuario };
            Produto = new Identificador { _Id = idProduto };
            NovaSenha = senha;
        }

        public Identificador Usuario { get; set; }
        public Identificador Produto { get; set; }
        public string NovaSenha { get; set; }
    }
}

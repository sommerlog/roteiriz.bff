﻿using Roteiriz.Data.Models.Roteiriz.Model.Identificador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class EditUserAccessKeyRequest
    {
        public int userId { get; set; }
        public int productId { get; set; }
        public bool active { get; set; }
        public bool blocked { get; set; }
    }

    public class EditarAccessKeyRequest
    {
        public EditarAccessKeyRequest(EditUserAccessKeyRequest model)
        {
            usuario = new Identificador { _Id = model.userId };
            produto = new Identificador { _Id = model.productId };
            ativo = model.active;
            bloqueado = model.blocked;
        }
        public Identificador usuario { get; set; }
        public Identificador produto { get; set; }
        public bool ativo { get; set; }
        public bool bloqueado { get; set; }
    }
}

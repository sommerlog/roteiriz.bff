﻿
namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class IAtivaMotoristaRequest
    {
        public int IdMotorista { get; set; }
        public int IdUsuario { get; set; }
    }
}

﻿

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class IEditaTabelaPrecoRequest
    {
        public int IdTabelaPreco { get; set; }
        public string CEPInicio { get; set; }
        public string CEPFinal { get; set; }
        public double Valor { get; set; }
        public int IdTipoVeiculo { get; set; }
        public bool Ativo { get; set; }
        public int? UsuarioAlteracao { get; set; }
    }
}

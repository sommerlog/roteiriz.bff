﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class LoginRequestModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class LoginRequest
    {
        public LoginRequest(string email, string password, int produto)
        {
            this.Key = email;
            this.Password = password;
            this.Controle = new ControleRequest(produto);
        }
        public string Key { get; set; }
        public string Password { get; set; }
        public ControleRequest Controle { get; set; }

    }
    public class ControleRequest
    {
        public ControleRequest(int produto)
        {
            Produto = new ProdutoRequest(produto);
        }
        public ProdutoRequest Produto { get; set; }
    }
    public class ProdutoRequest
    {
        public ProdutoRequest(int Id)
        {
            _Id = Id;
        }
        public int _Id { get; set; }
    }
}

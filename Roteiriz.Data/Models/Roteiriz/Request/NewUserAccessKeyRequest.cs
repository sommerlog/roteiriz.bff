﻿using Roteiriz.Data.Models.Roteiriz.Model.Identificador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public  class NewUserAccessKeyRequest
    {
        public int userId { get; set; }
        public int productId { get; set; }
        public string password { get; set; }
    }

    public class NovoAccessKeyRequest
    {
        public NovoAccessKeyRequest(NewUserAccessKeyRequest model)
        {
            usuario = new Identificador { _Id = model.userId };
            produto = new Identificador { _Id = model.productId };
            password = model.password;
        }
        public Identificador usuario { get; set; }
        public Identificador produto { get; set; }
        public string password { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class ImportaPlanilhaRequest
    {
        public string NomeArquivo { get; set; }
        public string Base64 { get; set; }
    }
}

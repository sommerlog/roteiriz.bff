﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class CadastraRotaRequest
    {
        public string NomeRota { get; set; }
        public bool VoltaCD { get; set; }
        public DateTime DataSaida { get; set; }
        public int IdMotorista { get; set; }
        public int IdVeiculoMotorista { get; set; }
        public RotaRequest Rota { get; set; }
    }
    public class RotaRequest
    {
        public int IdDepositoOrigem { get; set; }
        public DestinoRotaPedido Destino { get; set; }
        public IEnumerable<RotaPedido> Pedidos { get; set; }
    }
    public class DestinoRotaPedido
    {
        public int IdDepositoDestino { get; set; }
        public double DestinoLatitude { get; set; }
        public double DestinoLongitude { get; set; }
    }
    public class RotaPedido
    {
        public int IdPedido { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Ordem { get; set; }
    }
}

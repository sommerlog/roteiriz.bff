﻿
namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public  class ICadastraMotoristaRequest
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string CPF { get; set; }
        public string Telefone { get; set; }
        public string NrCNH { get; set; }
        public DateTime DataVencimentoCNH { get; set; }
        public DateTime DataVencimentoPesquisa { get; set; }
        public int QtdPedidosMaxima { get; set; }
        public int IdTipoMotorista { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Complemento { get; set; }
        public bool Ativo { get; set; }
        public int UsuarioCadastro { get; set; }
    }
}

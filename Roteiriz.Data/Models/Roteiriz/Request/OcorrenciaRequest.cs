﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class OcorrenciaRequest
    {
        public int IdOcorrencia { get; set; }
        public string Descricao { get; set; }
    }
}

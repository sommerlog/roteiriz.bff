﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class IClienteUsuarioRequest
    {
        public int idUsuario { get; set; }
        public int idCliente { get; set; }
        public bool acessoPrincipal { get; set; }
        public int idTipoAcesso { get; set; }
        public DateTime dataInativacaoAcesso { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Request
{
    public class IClienteRequest
    {
        public int idCliente { get; set; }
        public string nome { get; set; }
        public string inscricao { get; set; }
        public int idTipoPessoa { get; set; }
        public bool ativo { get; set; } 
        public string logo { get; set; } 
        public int usuarioInclusao { get; set; }
        public int usuarioAlteracao { get; set; }

    }
}

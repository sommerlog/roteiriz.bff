﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Cliente
{
    public class Cliente
    {
        public int idCliente { get; set; }
        public string nome { get; set; }
        public string inscricao { get; set; }
        public TipoPessoa tipoPessoa { get; set; }
        public bool ativo { get; set; }
        public string logo { get; set; }
        public UsuarioInclusao? usuarioInclusao { get; set; }
        public UsuarioAlteracao? usuarioAlteracao { get; set; }
    }
    public class TipoPessoa
    {
        public int idTipoPessoa { get; set; }
        public string descricao { get; set; }

    }

    public class UsuarioInclusao
    {
        public int idUsuario { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public DateTime dataInclusao { get; set; }

    }

    public class UsuarioAlteracao
    {
        public int idUsuario { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public DateTime dataInclusao { get; set; }

    }
}
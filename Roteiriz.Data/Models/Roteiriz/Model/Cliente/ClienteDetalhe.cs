﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Cliente
{
    public class ClienteDetalhe
    {
        public Cliente cliente { get; set; }
        public TipoAcesso tipoAcesso { get; set; }
        public bool acessoPrincipal { get; set; }
        public bool primeiroAcesso { get; set; }
        public DateTime? dataUltimoAcesso { get; set; }
        public DateTime? dataInativacao { get; set; }
        public DateTime dataCadastro { get; set; }
    }

    public class TipoAcesso
    {
        public int id { get; set;}
        public string descricao { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.TabelaPreco
{
    public class TabelaPreco
    {
        public int IdTabelaPreco { get; set; }
        public string CEPInicio { get; set; }
        public string CEPFinal { get; set; }
        public double Valor { get; set; }
        public int IdTipoVeiculo { get; set; }
        public bool Ativo { get; set; }
        public int UsuarioCadastro { get; set; }
        public string NomeUsuarioCadastro { get; set; }
        public DateTime DataCadastro { get; set; }
        public int? UsuarioAlteracao { get; set; }
        public string? NomeUsuarioAlteracao { get; set; }
        public DateTime? DataAlteracao { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Produto
{
    public class ProdutoDetalhe
    {
        public IProduto produto { get; set; }
        public int id { get; set; }
        public bool ativo { get; set; }
        public bool bloqueado { get; set; }
        public DateTime dataCadastro { get; set; }
        public int idUsuario { get; set; }
    }
}

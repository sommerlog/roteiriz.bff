﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Produto
{
    public class IProduto
    {
        public int idProduto { get; set; }
        public string descricao { get; set; }
        public string nome { get; set; }
        public bool ativo { get; set; }
    }
}

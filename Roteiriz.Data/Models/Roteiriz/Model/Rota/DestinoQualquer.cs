﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Rota
{
    public class DestinoQualquer
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}

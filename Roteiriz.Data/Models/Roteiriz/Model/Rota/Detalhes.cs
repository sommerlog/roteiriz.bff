﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Rota
{
    public class Detalhes
    {
        public double DuracaoTotalSegundos { get; set; }
        public double TrajetoTotalMetros { get; set; }
        public IEnumerable<DetalhePedido> Pedidos { get; set; }
    }
}

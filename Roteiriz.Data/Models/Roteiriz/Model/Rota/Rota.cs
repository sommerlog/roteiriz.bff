﻿using Roteiriz.Data.Models.Roteiriz.Model.Motorista;
using Roteiriz.Data.Models.Roteiriz.Model.VeiculoMotorista;
using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Rota
{
    public class Rota
    {
        public int IdCliente { get; set; }
        public int IdRota { get; set; }
        public string Nome { get; set; }
        public DateTime DataSaida { get; set; }
        public int IdMotorista { get; set; }
        public IMotorista Motorista { get; set; }
        public int IdVeiculoMotorista { get; set; }
        public IVeiculoMotorista VeiculoMotorista { get; set; }
        public bool VoltaCD { get; set; }
        public string LatitideOrigem { get; set; }
        public string LongitudeOrigem { get; set; }
        public string LatitideDestino { get; set; }
        public string LongitudeDestino { get; set; }
        public int IdRotaStatus { get; set; }

        public List<RotaPedidos> Pedidos { get; set; }
    }

    public class RotaStatus
    {
        public int IdRotaStatus { get; set; }
        public string Descricao { get; set; }
    }

    public class RotaPedidos
    {
        public int IdRota { get; set; }
        public int IdPedido { get; set; }
        public IPedidosRotasResponse Pedido { get; set; }
        public int Ordem { get; set; }
        public int Conferido { get; set; }
    }
}

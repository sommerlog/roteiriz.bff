﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Rota
{
    public class DetalhePedido
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int IdPedido { get; set; }
        public int Ordem { get; set; }
    }
}

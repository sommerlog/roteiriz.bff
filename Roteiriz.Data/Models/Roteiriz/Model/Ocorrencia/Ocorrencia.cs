﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Ocorrencia
{
    public class Ocorrencia
    {
        public int IdOcorrencia { get; set; }
        public int IdCliente { get; set; }
        public string Descricao{ get; set; }
    }
}

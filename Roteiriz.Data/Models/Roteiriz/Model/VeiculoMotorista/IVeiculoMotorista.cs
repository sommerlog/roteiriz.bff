﻿using Roteiriz.Data.Models.Roteiriz.Model.Veiculo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.VeiculoMotorista
{
    public class IVeiculoMotorista
    {
        public int IdVeiculoMotorista { get; set; }
        public int IdVeiculo { get; set; }
        public int IdMotorista { get; set; }
        public bool VeiculoPadrao { get; set; }
        public DateTime DataAtribuicao { get; set; }
        public IVeiculo Veiculo { get; set; }
    }
}

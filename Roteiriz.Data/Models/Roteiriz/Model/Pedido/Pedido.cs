﻿using Roteiriz.Data.Models.Roteiriz.Model.Rota;
using Roteiriz.Data.Models.Roteiriz.Model.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Pedido
{
    public class Pedido
    {
        public int idRemessaPedido { get; set; }
        public int idCliente { get; set; }
        public int IdPedido { get; set; }
        public int IdDeposito { get; set; }
        public string Remetente { get; set; }
        public int IdRegiao { get; set; }
        public int CTE_Minuta { get; set; }
        public string NrPedido { get; set; }
        public double Volume { get; set; }
        public double PesoTaxa { get; set; }
        public int NrNotaFiscal { get; set; }
        public int NrNFSe { get; set; }
        public string ChaveNFe { get; set; }
        public double ValorNF { get; set; }
        public DateTime DataFrete { get; set; }
        public DateTime PrevisaoEntrega { get; set; }
        public double TotalReceber { get; set; }
        public int IdStatusPedido { get; set; }
        public DateTime DataImportacao { get; set; }
        public Deposito Deposito { get; set; }
        public Regiao Regiao { get; set; }
    }
}

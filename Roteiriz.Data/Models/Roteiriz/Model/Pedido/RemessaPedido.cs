﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Pedido
{
    public class RemessaPedido
    {
        public int IdCliente { get; set; }
        public int IdRemessaPedido { get; set; }
        public string NomeArquivoOriginal { get; set; }
        public string UrlArquivoCloud { get; set; }
        public bool IndProcessada { get; set; }
        public DateTime DataImportacao { get; set; }
        public int UsuarioImportacao { get; set; }
    }
}

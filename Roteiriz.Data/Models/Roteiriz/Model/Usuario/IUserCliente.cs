﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Usuario
{
    public class IUserCliente
    {
        public int Id { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public ITipoAcesso tipoAcesso { get; set; } 
        public bool acessoPrincipal { get; set; }
        public bool pimeiroAcesso { get; set; }
        public DateTime? dataUltimoAcesso { get; set; }
        public DateTime? dataInativacao { get; set; }
        public DateTime dataCadastro { get; set; }


    }

    public class ITipoAcesso
    {
        public int Id { get; set; }
        public string descricao { get; set; }

    }
}
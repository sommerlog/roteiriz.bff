﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Usuario
{
    public class IUser
    {
        public int _Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}

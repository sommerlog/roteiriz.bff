﻿using Roteiriz.Data.Models.Roteiriz.Model.Cliente;
using Roteiriz.Data.Models.Roteiriz.Model.Produto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Usuario
{
    public class IDetalheUsuario
    {
        public IUsuario usuario { get; set; }
        public IEnumerable<ClienteDetalhe> clientes { get; set; }
        public IEnumerable<ProdutoDetalhe> produtos { get; set; }
    }
}

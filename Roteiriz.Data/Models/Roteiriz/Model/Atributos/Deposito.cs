﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Atributos
{
    public class Deposito
    {
        public int IdDeposito { get; set; }
        public int IdCliente { get; set; }
        public string Descricao{ get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Endereço { get; set; }
    }
}

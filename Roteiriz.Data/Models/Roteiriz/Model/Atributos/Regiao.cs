﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Atributos
{
    public class Regiao
    {
        public int IdRegiao { get; set; }
        public int IdCliente { get; set; }
        public string Sigla { get; set; }
    }
}

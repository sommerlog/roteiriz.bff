﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Model.Veiculo
{
    public class TipoVeiculo
    {
        public int IdTipoVeiculo { get; set; }
        public string Categoria { get; set; }
        public string Icone { get; set; }
        public double Peso { get; set; }
        public double Valor { get; set; }
        public double Cubagem_Diametro { get; set; }
        public double Cubagem_Largura { get; set; }
        public double Cubagem_Altura { get; set; }
        public bool Ativo { get; set; }
        public int UsuarioCadastro { get; set; }
        public string NomeUsuarioCadastro { get; set; }
        public DateTime DataCadastro { get; set; }
        public int? UsuarioAlteracao { get; set; }
        public string? NomeUsuarioAlteracao { get; set; }
        public DateTime? DataAlteracao { get; set; }
    }
}

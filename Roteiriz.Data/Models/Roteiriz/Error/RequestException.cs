﻿using Roteiriz.Data.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Error
{
    public class RequestException<TResponse> : BaseApiException<TResponse>
        where TResponse : class
    {
        public RequestException(TResponse response)
            :base(response)
        {
            
        }
    }
}

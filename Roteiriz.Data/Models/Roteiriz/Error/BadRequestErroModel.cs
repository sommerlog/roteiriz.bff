﻿using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Error
{
    public class BadRequestErroModel
    {
        public ErrorResponse Error { get; set; }
        public ControleResponse Controle { get; set; }

        public class ErrorResponse
        {
            public string Message { get; set; }
        }
    }
}

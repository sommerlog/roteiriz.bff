﻿using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.Veiculo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IConsultaVeiculoResponse : ResponseModel
    {
        public IVeiculo Veiculo { get; set; }
        public IEnumerable<IVeiculo> Veiculos { get; set; }
    }
}

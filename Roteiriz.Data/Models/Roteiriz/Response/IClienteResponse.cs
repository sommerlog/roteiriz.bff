﻿using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IClienteResponse: ResponseModel
    {
        public Cliente Cliente { get; set; }

        public IEnumerable<Cliente> Clientes { get; set; }
    }
}

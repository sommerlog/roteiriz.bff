﻿using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.TabelaPreco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IConsultaTabelaPrecoResponse : ResponseModel
    {
        public TabelaPreco TabelaPreco { get; set; }
        public IEnumerable<TabelaPreco> TabelasPreco { get; set; }
    }
}

﻿

using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.Motorista;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IConsultaMotoristaResponse : ResponseModel
    {
        public IMotorista Motorista { get; set; }
        public IEnumerable<IMotorista> Motoristas { get; set; }
    }
}

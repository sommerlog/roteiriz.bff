﻿

using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.Pedido;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public  class IConsultaPedidoResponse : ResponseModel
    {
        public Pedido Pedido { get; set; }
        public IEnumerable<Pedido> Pedidos { get; set; }
    }
}

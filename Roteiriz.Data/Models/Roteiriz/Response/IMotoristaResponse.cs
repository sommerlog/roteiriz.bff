﻿
using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.Identificador;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IMotoristaResponse : ResponseModel
    {
        public Identificador Motorista { get; set; }
    }
}

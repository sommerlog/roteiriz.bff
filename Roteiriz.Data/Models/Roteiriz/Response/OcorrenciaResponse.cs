﻿using Roteiriz.Data.Models.Roteiriz.Model.Atributos;
using Roteiriz.Data.Models.Roteiriz.Model.Ocorrencia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class OcorrenciaResponse
    {
        public IEnumerable<Ocorrencia> Ocorrencias { get; set; }
        public Ocorrencia Ocorrencia { get; set; }
        public ControleResponse Controle { get; set; }
    }
}

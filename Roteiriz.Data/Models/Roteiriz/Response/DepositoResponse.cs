﻿using Roteiriz.Data.Models.Roteiriz.Model.Atributos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class DepositoResponse
    {
        public IEnumerable<Deposito> Depositos { get; set; }
        public Deposito Deposito { get; set; }
        public ControleResponse Controle { get; set; }
    }
}

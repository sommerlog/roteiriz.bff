﻿using Roteiriz.Data.Models.Roteiriz.Model.Produto;
using System.Collections.Generic;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class ILoginUsuarioResponse
    {
        public int _Id { get; set; }
        public int Name { get; set; }
        public int Email { get; set; }
        public int MaskedEmail { get; set; }
        public List<IUserAgent> Agents { get; set; }
        public List<IUserProduct> Products { get; set; }
        public IUserProduct AccessToken { get; set; }
    }

    public class IUserAgent
    {
        public int MyProperty { get; set; }
    }

    public class IUserProduct
    {
        public IProduto product { get; set; }
    }

    public class IAccessToken
    {
        public string Token { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime CreationDate { get; set; }
    }
}

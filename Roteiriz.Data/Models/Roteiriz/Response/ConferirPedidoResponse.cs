﻿using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.Motorista;
using Roteiriz.Data.Models.Roteiriz.Model.Pedido;
using Roteiriz.Data.Models.Roteiriz.Model.Rota;
using Roteiriz.Data.Models.Roteiriz.Model.Veiculo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class ConferirPedidoResponse : ResponseModel
    {
        public PedidoConferenciaResponse Pedido { get; set; }
    }

    public class PedidoConferenciaResponse
    {
        public Pedido Pedido { get; set; }
        public IClientePedidoResponse Cliente { get; set; }
        public Rota Rota { get; set; }
        public IMotorista Motorista { get; set; }
        public IVeiculo Veiculo { get; set; }
        public int VolumeConferido { get; set; }
        public int OrdemEntrega { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class ControleResponse
    {
        public string Prefix { get; set; }
        public string Message { get; set; }
    }
}

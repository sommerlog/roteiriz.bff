﻿using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.Rota;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class ConsultaRotaResponse : ResponseModel
    {
        public Rota Rota { get; set; }
        public IEnumerable<Rota> Rotas { get; set; }
    }
}

﻿using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.Pedido;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class ImportaPlanilhaResponse : ResponseModel
    {
        public string Mensagem{ get; set; }
        public IEnumerable<RemessaPedido> Planilhas { get; set; }

    }
}

﻿using Roteiriz.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class LoginResponseModel : ResponseModel
    {
        public LoginResponse user { get; set; }
    }
    public class LoginResponse
    {
        public int _Id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string maskedEmail { get; set; }
        public List<AgentList> agents { get; set; }
        public List<ProductList> products { get; set; }
        public AccessToken accessToken { get; set; }

    }

    public class Access
    {
        public int _Id { get; set; }
        public string description { get; set; }
        public bool mainAccess { get; set; }
        public bool firstAccess { get; set; }
        public DateTime? lastAccess { get; set; }
        public DateTime? inactivationAccessDate { get; set; }
        public DateTime signUpDate { get; set; }
    }

    public class AccessToken
    {
        public string token { get; set; }
        public DateTime expirationDate { get; set; }
        public DateTime creationDate { get; set; }
    }

    public class AgentList
    {
        public Agent agent { get; set; }
    }

    public class Agent
    {
        public int _Id { get; set; }
        public string name { get; set; }
        public bool active { get; set; }
        public string logo { get; set; }
        public PersonCategory personCategory { get; set; }
        public Document document { get; set; }
        public Access access { get; set; }
    }

    public class Document
    {
        public int _Id { get; set; }
        public string document { get; set; }
        public string maskedDocument { get; set; }
    }

    public class PersonCategory
    {
        public int _Id { get; set; }
        public string description { get; set; }
    }

    public class ProductList
    {
        public Product product { get; set; }
    }

    public class Product
    {
        public int _Id { get; set; }
        public string description { get; set; }
        public string name { get; set; }
    }
}

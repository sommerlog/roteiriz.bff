﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IClientePedidoResponse
    {
        public int IdClientePedido { get; set; }
        public int IdPedido { get; set; }
        public string CodigoExterno { get; set; }
        public string Nome { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Bairro { get; set; }
        public string Cep { get; set; }
        public string Cidade { get; set; }
        public string UF { get; set; }
        public string Complemento { get; set; }
        public string ObservacaoEntrega { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}

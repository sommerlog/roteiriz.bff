﻿using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.VeiculoMotorista;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IConsultaVeiculoMotoristaResponse : ResponseModel
    {
        public IVeiculoMotorista VeiculoMotorista { get; set; }
        public IEnumerable<IVeiculoMotorista> VeiculosMotorista { get; set; }
    }
}

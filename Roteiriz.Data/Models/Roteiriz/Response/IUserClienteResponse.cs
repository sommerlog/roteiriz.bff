﻿using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.Usuario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IUserClienteResponse : ResponseModel
    {
        public  IEnumerable<IUserCliente> usuarios { get; set; }
        public IEnumerable<IUserCliente> usuario { get; set; }
    }
}

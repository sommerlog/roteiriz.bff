﻿
using Roteiriz.Data.Interfaces;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IConsultaEnderecoPedidoResponse : ResponseModel
    {
        public IEnumerable<IPedidosRotasResponse> PedidoRotas { get; set; }
    }
}

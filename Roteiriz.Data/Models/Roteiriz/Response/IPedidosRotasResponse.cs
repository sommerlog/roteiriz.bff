﻿using Roteiriz.Data.Models.Roteiriz.Model.Pedido;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IPedidosRotasResponse
    {
        public IClientePedidoResponse ClientePedido { get; set; }
        public Pedido Pedido { get; set; }
    }
}

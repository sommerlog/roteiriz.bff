﻿

using Roteiriz.Data.Interfaces;
using Roteiriz.Data.Models.Roteiriz.Model.Rota;

namespace Roteiriz.Data.Models.Roteiriz.Response
{
    public class IDetalheRotaResponse : ResponseModel
    {
        public Detalhes Detalhes { get; set; }
    }
}

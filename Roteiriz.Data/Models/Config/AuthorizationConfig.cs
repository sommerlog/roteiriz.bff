﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Config
{
    public class AuthorizationConfig
    {
        public List<ServiceUsers> Usuarios { get; set; }
        public class ServiceUsers
        {
            public string Usuario { get; set; }
            public string Senha { get; set; }
            public RoteirizAuthorization RoteirizAuthorization { get; set; }
        }
        public class RoteirizAuthorization
        {
            public string Usuario { get; set; }
            public string Senha { get; set; }
            public int CodigoProduto { get; set; }
            public string SiglaProduto { get; set; }
        }
    }
}

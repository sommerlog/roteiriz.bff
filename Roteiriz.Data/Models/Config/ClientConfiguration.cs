﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models.Config
{
    public class ClientConfiguration
    {
        public AuthorizationConfiguration Authorization { get; set; }
        public string BaseAddress { get; set; }
        public string ContentType { get; set; } = "application/json";
        public EndPointCollection Endpoints { get; set; }
        public ReplaceConfiguration[] Replaces { get; set; }
        public TimeSpan RequestTimeout { get; set; } = TimeSpan.FromSeconds(60);
        public string UserAgent { get; set; } = "Roteiriz.Roteirizador.ClientAPI";


        public class AuthorizationConfiguration
        {
            public string User { get; set; }
            public string Password { get; set; }
            public int CodigoProduto { get; set; }
        }

        public class EndPointCollection
        {
            public string AuthSchema { get; set; }
            public string Roteirizador { get; set; }
            public string Gerenciador { get; set; }
        }

        public class ReplaceConfiguration
        {
            public string From { get; set; }
            public string To { get; set; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Models
{
    public class TokenModel
    {
        public string Token { get; set; }
        public DateTime? DataExpiracao { get; set; }
    }
    public class TokenServiceConfig
    {
        public string CacheId { get; set; }
    }
}

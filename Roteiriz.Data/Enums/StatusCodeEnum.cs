﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Enums
{
    public enum StatusCodeEnum
    {

        InternalError = 500,

        Unauthorized = 401,

        BadRequest = 400,

        NotFound = 404,

        DeviceNotSuported = 405,

        UpdateRequired = 40502
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Enums
{
    public enum ApiEndPointEnum
    {
        AuthSchema,
        Roteirizador,
        Gerenciador
    }
}

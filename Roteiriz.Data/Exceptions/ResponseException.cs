﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Exceptions
{
    public class ResponseException : Exception
    {
        public string ResponseString { get; set; }
    }
}

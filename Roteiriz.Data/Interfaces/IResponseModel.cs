﻿using Roteiriz.Data.Models.Roteiriz.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Roteiriz.Data.Interfaces
{
    public class ResponseModel : IResponseModel
    {
        public ControleResponse Controle { get; set; }
    }
    public interface IResponseModel
    {
        public ControleResponse Controle { get; set; }
    }
}
